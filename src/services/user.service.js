import axios from 'axios';

export default class UserService{

    /**
     * Auth user and return token
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async auth(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/users/auth`, body);
    }
}
