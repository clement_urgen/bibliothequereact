import React, {Component} from 'react';
import AuteurService from "../../services/auteur.service";

export default class AuteurDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            auteur: null,
            nom: null,
            prenom: null,
            naissance: null,
            description: null,
            mort: null,
            image: null
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleChangeThumbnail(e){
        this.setState({
            image: e.target.files[0]
        });
    }

    async componentDidMount() {
        //Récupération de l'id passé dans l'URL
        let {id} = this.props.match.params;

        //Requête pour récupérer tous les auteurs
        let response = await AuteurService.list();
        let {auteurs} = response.data;
        let auteur = auteurs.find(item => item._id === id);
        this.setState({auteur: auteur, nom: auteur.nom, prenom: auteur.prenom, description: auteur.description, naissance: auteur.naissance, mort: auteur.mort, image: auteur.image});
    }

    async submit(e){
        e.preventDefault();
    try{
        let id = this.state.auteur._id;
        let body = {nom: this.state.nom, prenom: this.state.prenom, description: this.state.description, naissance: this.state.naissance, image: this.state.image, mort: this.state.mort};
        let image = this.state.image;
            await AuteurService.update(id, body);
            await AuteurService.updateImage(id, body);
            this.props.history.push('/auteurs');
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {auteur, nom, prenom, description, naissance, mort, image} = this.state;
        return <div className="container-fluid">
            <h1>Update de l'auteur {auteur && auteur.nom}</h1>

            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Nom</label>
                    <input type="text"
                           id="nom"
                           required
                           value={nom && nom}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                    <label>Prénom</label>
                    <input type="text"
                           id="prenom"
                           value={prenom && prenom}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                    <label>Description</label>
                    <input type="area"
                           id="description"
                           required
                           value={description && description}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                    <label>image</label>
                    <input type="file"
                           id="image"
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                    <label>Naissance</label>
                    <input type="text"
                           id="naissance"
                           required
                           value={naissance && naissance}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                    <label>Mort</label>
                    <input type="text"
                           id="mort"
                           value={mort && mort}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                </div>

                <button type="submit" className="btn btn-primary">Update</button>
            </form>

        </div>
    }
}
