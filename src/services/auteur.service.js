import axios from 'axios';

export default class AuteurService{

    /**
     * Get author's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/auteurs`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Create one author
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/auteurs`, body,{
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Update one author
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/auteurs/${id}`, body,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    static async updateImage(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/auteurs/${id}`, body,{
            headers: {
                'Content-Type': 'multipart/form-data',                
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Delete one author
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/auteurs/${id}`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }
}
