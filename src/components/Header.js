import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {updateUser} from "../actions/users.actions";

class Header extends Component{

    logout(){
        localStorage.removeItem('tokenbibliotheque');
        this.props.updateUser(null);
    }

    render() {
        return <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/">Bibliothèque</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    {
                        this.props.user !== null && <div className="navbar-nav">
                            <Link to={'/genres'} className="nav-link">Genres</Link>
                            <Link to={'/categories'} className="nav-link">Categories</Link>
                            <Link to={'/editions'} className="nav-link">Editions</Link>
                            <a className="nav-link" href="#">Users</a>

                        </div>
                    }

                    <Link to={'/auteurs'} className="nav-link">Auteurs</Link>
                    <Link to={'/livres'} className="nav-link">Livres</Link>
                    {
                        this.props.user !== null ?
                            <div className="nav-link" onClick={() => this.logout()}>Logout</div>
                            : <Link to={'/login'} className="nav-link">Login</Link>
                    }
                </div>
            </div>
        </nav>
    }
}

const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))}
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
