import React, {Component} from 'react';
import GenreService from "../../services/genre.service";
import LivreService from "../../services/livre.service";
import AuteurService from "../../services/auteur.service";
import EditionService from "../../services/edition.service";
import CategorieService from "../../services/categorie.service";

export default class LivresUpdate extends Component{

    constructor(props) {
        super(props);
        this.state = {
            livre: null,
            titre: null,
            parution: null,
            synopsis: null,
            couverture: null,
            lien: null,

            activatedGenres: [],
            activatedAuteurs: [],
            activatedEditions: [],
            activatedCategories: [],

            genres: [],
            auteurs: [],
            editions: [],
            categories: [],
        }
    }


    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeThumbnail(e){
        this.setState({
            couverture: e.target.files[0]
        });
    }

    async componentDidMount() {
    
            let {id} = this.props.match.params;
            let responseGenre = await GenreService.list();
            let responseEdition = await EditionService.list();
            let responseCategorie = await CategorieService.list();
            let responseAuteur = await AuteurService.list();
            let response = await LivreService.list();
            let {livres} = response.data;
            let livre = livres.find(item => item._id === id);

            this.setState({livre: livre, 
                titre: livre.titre, 
                parution: livre.parution, 
                synopsis: livre.synopsis, 
                lien: livre.lien,
                couverture: livre.couverture,
                genres: responseGenre.data.genres,
                activatedGenres: livre.genres[0],
                editions: responseEdition.data.editions,
                activatedEditions: livre.edition[0],
                categories: responseCategorie.data.categories,
                activatedCategories: livre.categorie[0],
                auteurs: responseAuteur.data.auteurs,
                activatedAuteurs: livre.auteur[0],
            });
        
    }

    async submit(e){
        e.preventDefault();
    try{
        let id = this.state.livre._id;
        let couverture = this.state.couverture;
        let body = {titre: this.state.titre, parution: this.state.parution, synopsis: this.state.synopsis, lien: this.state.lien, genres: this.state.activatedGenres, auteur: this.state.activatedAuteurs, edition: this.state.activatedEditions, categorie: this.state.activatedCategories};
            await LivreService.update(id, body);
            let formData = new FormData();
            formData.append('couverture', couverture);
            await LivreService.updateImage(id, formData);
            this.props.history.push('/livres');
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        
        let {livre, titre, parution, synopsis, couverture, lien, genres, auteurs, editions, categories, activatedGenres, activatedAuteurs, activatedEditions, activatedCategories} = this.state;
        return <div className="container-fluid">
            <h1>Update du Livre {titre}</h1>

            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Titre</label>
                    <input type="text" id="titre" required  value={titre} className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                </div>

                <div className="form-group">
                    <label>Auteur</label>
                    <select id="activatedAuteurs" required className="form-control"
                            onChange={(e) => this.handleChange(e)}>
                    <option value={activatedAuteurs._id}>{activatedAuteurs.prenom} {activatedAuteurs.nom}</option>

                        {
                            auteurs.map((auteur, index) => {
                                return <option key={index} value={auteur._id}>{auteur.nom} {auteur.prenom}</option>
                            })
                        }
                    </select>
                </div>

                <div className="form-group">
                    <label>Synopsis</label>
                    <textarea id="synopsis" value={synopsis} className="form-control"
                           onChange={(e) => this.handleChange(e)}/>
                </div>

                <div className="form-group">
                    <label>Edition</label>
                    <select id="activatedEditions" required className="form-control"
                            onChange={(e) => this.handleChange(e)}>
                        <option value={activatedEditions._id}>{activatedEditions.nom}</option>
                        {
                            editions.map((edition, index) => {
                                return <option key={index} value={edition._id}>{edition.nom}</option>
                            })
                        }
                    </select>
                </div>

                <div className="form-group">
                    <label>Parution</label>
                    <input type="text" id="parution" required value={parution} className="form-control"
                           onChange={(e) => this.handleChange(e)}/>
                </div>

                <div className="form-group">
                    <label>Couverture</label>
                    <input type="file" id="couverture" accept="image/jpeg, image/png"
                             className="form-control"
                            onChange={(e) => this.handleChangeThumbnail(e)}/>
                </div>

                <div className="form-group">
                    <label>Lien</label>
                    <input type="text" id="lien" required value={lien} className="form-control"
                           onChange={(e) => this.handleChange(e)}/>
                </div>


                <div className="row">

                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Genres</label>
                            <select id="activatedGenres" required className="form-control"
                                    onChange={(e) => this.handleChange(e)}>
                            <option value={activatedGenres._id}>{activatedGenres.libelle}</option>
                                {
                                    genres.map((genre, index) => {
                                        return <option key={index} value={genre._id}>{genre.libelle}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Categorie</label>
                            <select id="activatedCategories" required className="form-control"
                                    onChange={(e) => this.handleChange(e)}>
                                    <option value={activatedCategories._id}>{activatedCategories.libelle}</option>
                                {
                                    categories.map((categorie, index) => {
                                        return <option key={index} value={categorie._id}>{categorie.libelle}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>

                </div>

                <button type="submit" className="btn btn-primary">Modifier</button>
            </form>

        </div>
    }
}
