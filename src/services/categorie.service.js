import axios from 'axios';

export default class CategorieService{

    /**
     * Get categorie's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/categories`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Create one categorie
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/categories`, body,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Update one categorie
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/categories/${id}`, body,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Delete one categorie
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/categories/${id}`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }
}
