import React, {Component} from 'react';
import GenreService from "../../services/genre.service";

export default class GenreDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            genre: null,
            libelle: null,
        }
    }

    async componentDidMount() {
        //Récupération de l'id passé dans l'URL
        let {id} = this.props.match.params;

        //Requête pour récupérer tous les genres
        let response = await GenreService.list();
        let {genres} = response.data;
        let genre = genres.find(item => item._id === id);
        this.setState({genre: genre, libelle: genre.libelle});
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    async submit(e){
        e.preventDefault();

        try{
            let id = this.state.genre._id;
            let body = {libelle: this.state.libelle};
            await GenreService.update(id, body);
            this.props.history.push('/genres');
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {genre, libelle} = this.state;
        return <div className="container-fluid">
            <h1>Update du genre {genre && genre.libelle}</h1>

            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Genre</label>
                    <input type="text"
                           id="libelle"
                           required
                           value={libelle && libelle}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                </div>

                <button type="submit" className="btn btn-primary">Update</button>
            </form>

        </div>
    }

}
