import axios from 'axios';

export default class LivreService{

    /**
     * Get list of livres
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/livres`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Delete one livre
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/livres/${id}`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Create one livre
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/livres`, body, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Update one livre
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/livres/update/${id}`, body,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    static async updateImage(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/livres/update/${id}`, body,{
            headers: {
                'Content-Type': 'multipart/form-data',                
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

}
