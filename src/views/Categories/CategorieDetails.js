import React, {Component} from 'react';
import CategorieService from "../../services/categorie.service";

export default class CategorieDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            categorie: null,
            libelle: null,
        }
    }

    async componentDidMount() {
        //Récupération de l'id passé dans l'URL
        let {id} = this.props.match.params;

        //Requête pour récupérer tous les categories
        let response = await CategorieService.list();
        let {categories} = response.data;
        let categorie = categories.find(item => item._id === id);
        this.setState({categorie: categorie, libelle: categorie.libelle});
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    async submit(e){
        e.preventDefault();

        try{
            let id = this.state.categorie._id;
            let body = {libelle: this.state.libelle};
            await CategorieService.update(id, body);
            this.props.history.push('/categories');
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {categorie, libelle} = this.state;
        return <div className="container-fluid">
            <h1>Update de la categorie {categorie && categorie.libelle}</h1>

            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Categorie</label>
                    <input type="text"
                           id="libelle"
                           required
                           value={libelle && libelle}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                </div>

                <button type="submit" className="btn btn-primary">Update</button>
            </form>

        </div>
    }

}
