import React, {Component} from 'react';
import AuteurService from "../../services/auteur.service";
import moment from 'moment';
import {Link} from 'react-router-dom';
import {updateUser} from "../../actions/users.actions";
import {connect} from "react-redux";

class AuteurList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            auteurs: [],
            auteur: null, //La nouvelle valeur du auteur qu'on veut créer
            updatedAuteur: {id: null, nom: null, prenom: null, description: null, naissance: null, mort: null}
        }
    }

    async componentDidMount() {
        try{
            let response = await AuteurService.list();
            this.setState({auteurs: response.data.auteurs});
        }catch (e) {
            console.error(e);
        }
    }

    async submit(e){
        e.preventDefault();
        let {nom, prenom, description, naissance, mort, image} = this.state;

        let formData = new FormData();
        formData.append('nom', nom);
        formData.append('prenom', prenom);
        formData.append('description', description);
        formData.append('naissance', naissance);
        formData.append('mort', mort);
        formData.append('image', image);

        try{
            await AuteurService.create(formData);
            this.props.history.push('/auteurs');
        }catch (e) {
            console.error(e);
        }
    }

    async submitUpdate(e){
        e.preventDefault();
        try {
            let body = {nom: this.state.updatedAuteur.nom, prenom: this.state.updatedAuteur.prenom, description: this.state.updatedAuteur.description, naissance: this.state.updatedAuteur.naissance, mort: this.state.updatedAuteur.mort};
            let response = await AuteurService.update(this.state.updatedAuteur.id, body);

            /**
             * Traitement pour ajouter dynamiquement ton nouveau auteur dans le state
             */

        }catch (e) {
            console.error(e);
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeThumbnail(e){
        this.setState({
            image: e.target.files[0]
        });
    }

    handleClickBtnModal(auteur){
        this.setState({updatedAuteur: {id: auteur._id, nom: auteur.nom, prenom: auteur.prenom, descrption: auteur.description, naissance: auteur.naissance, mort: auteur.mort}});
    }

    handleChangeUpdate(e){
        this.setState({
            updatedAuteur: {...this.state.updatedAuteur, nom: e.target.value, prenom: e.target.value, description: e.target.value, naissance: e.target.value, mort: e.target.value}
        });
    }

    async deleteAuteur(id){
        try{
            await AuteurService.delete(id);
            let {auteurs} = this.state;
            let newAuteurs = auteurs.filter(auteur => auteur._id.toString() !== id.toString());
            this.setState({auteurs: newAuteurs});
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {auteurs} = this.state;
        return <div className="container-fluid">
                <h1>Liste des auteurs</h1>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Description</th>
                            <th>Naissance</th>
                            <th>Mort</th>
                            <th>Image</th>
                            {
                                this.props.user !== null && <th>Actions</th>
                            }
                        </tr>
                    </thead>
                    <tbody>
                    {
                        auteurs.map((auteur, index) => {
                            return <tr key={index}>
                                <td>{auteur.nom}</td>
                                <td>{auteur.prenom}</td>
                                <td>{auteur.description}</td>
                                <td>{moment(auteur.naissance).format('D/M/Y')}</td>
                                <td>{moment(auteur.mort).format('D/M/Y')}</td>
                                <td><img className="card-img-top imageList" src={`${process.env.REACT_APP_HOST_API}/${auteur.image}`} alt="Card image cap"/></td>
                                {
                                    this.props.user !== null && <td>
                                        <Link to={`/auteurs/${auteur._id}`}>Update</Link>

                                        <button className="btn btn-danger"
                                                onClick={() => this.deleteAuteur(auteur._id)}>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                                 fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fillRule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </td>
                                }
                            </tr>
                        })
                    }
                    </tbody>
                </table>

            {
                this.props.user !== null && <div>
                    <h1>Ajouter un auteur</h1>

                    <form onSubmit={(e) => this.submit(e)}>
                        <div className="form-group">
                            <label>Nom</label>
                            <input type="text" id="nom" required className="form-control"
                                   onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>Prénom</label>
                            <input type="text" id="prenom" className="form-control"
                                   onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>Description</label>
                            <textarea id="description" required className="form-control" rows="5"
                                      onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>Image</label>
                            <input type="file" id="Image" accept="image/jpeg, image/png"
                                   className="form-control"
                                   onChange={(e) => this.handleChangeThumbnail(e)}/>
                        </div>

                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label>Naissance</label>
                                    <input type="date" id="naissance" required className="form-control"
                                           onChange={(e) => this.handleChange(e)}/>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label>Mort</label>
                                    <input type="date" id="mort" className="form-control"
                                           onChange={(e) => this.handleChange(e)}/>
                                </div>
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary">Ajouter</button>

                    </form>
                </div>

            }
            </div>
    }
}
const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))}
};

export default connect(mapStateToProps, mapDispatchToProps)(AuteurList);