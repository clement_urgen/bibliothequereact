import React, {Component} from 'react';
import LivreService from "../../services/livre.service";
import moment from 'moment';
import {Link} from 'react-router-dom';
import {updateUser} from "../../actions/users.actions";
import {connect} from "react-redux";

class LivresList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            livres: []
        }
    }

    async componentDidMount() {
        try{
            let response = await LivreService.list();
            this.setState({livres: response.data.livres});
        }catch (e) {
            console.error(e);
        }
    }

    async deleteLivre(id){
        try{
            await LivreService.delete(id);

            let response = await LivreService.list();
            this.setState({livres: response.data.livres});
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {livres} = this.state;
        console.log(livres)
        return <div className="container-fluid">
                <h1>Liste des livres</h1>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Parution</th>
                            <th>Synopsis</th>
                            <th>Lien</th>
                            <th>Genres</th>
                            <th>Maison d'edition</th>
                            <th>Categorie</th>
                            <th>Auteur</th>
                            <th>Couverture</th>
                            {
                                this.props.user !== null && <th>Actions</th>
                            }

                        </tr>
                    </thead>
                    <tbody>
                    {
                        livres.map((livre, index) => {
                            return <tr key={index}>
                                <td>{livre.titre}</td>
                                <td>{livre.parution}</td>
                                <td>{livre.synopsis}</td>
                                <td>{livre.lien}</td>
                                <td>{livre.genres[0].libelle}</td>
                                <td>{livre.edition[0].nom}</td>
                                <td>{livre.categorie[0].libelle}</td>
                                <td>{livre.auteur[0].prenom} {livre.auteur.nom}</td>
                                <td><img className="imageList card-img-top" src={`${process.env.REACT_APP_HOST_API}/${livre.couverture}`} alt="Card image cap"/></td>
                                
                                {
                                    this.props.user !== null && <td>
                                        <Link to={`/livres/update/${livre._id}`}>Update</Link>

                                        {/*<button type="button" className="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#modalUpdate"
                                            onClick={() => this.handleClickBtnModal(livre)}>Update</button>*/}

                                        <button className="btn btn-danger" onClick={() => this.deleteLivre(livre._id)}>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                                 fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fillRule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </td>
                                }
                            </tr>
                        })
                    }
                    </tbody>
                </table>
                {
                this.props.user !== null && <Link to={'/livres/add'} className="btn btn-primary">Ajouter un livre</Link>
            }
            </div>
    }
}
const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))}
};

export default connect(mapStateToProps, mapDispatchToProps)(LivresList);
