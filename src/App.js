import React, {Component} from 'react';
import {connect} from 'react-redux';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';
import Home from "./views/Home";
import Header from "./components/Header";
import GenresList from "./views/Genres/GenresList";
import GenreDetails from "./views/Genres/GenreDetails";
import CategoriesList from "./views/Categories/CategoriesList";
import CategorieDetails from "./views/Categories/CategorieDetails";
import EditionsList from "./views/Editions/EditionsList";
import EditionDetails from "./views/Editions/EditionDetails";
import AuteursList from "./views/Auteurs/AuteursList";
import AuteurDetails from "./views/Auteurs/AuteurDetails";
import LivresList from "./views/Livres/LivresList";
import LivresAdd from "./views/Livres/LivresAdd";
import LivresUpdate from "./views/Livres/LivresUpdate";
import LivresDetails from "./views/Livres/LivresDetails";


import Login from "./views/Login";

class App extends Component {
  render() {
        return <BrowserRouter>

            <Header />

            {
                this.props.user && this.props.user.role === 10 && <div>
                    {/*GENRES*/}
                    <Route exact path="/genres" component={GenresList}/>
                    <Route exact path="/genres/:id" component={GenreDetails}/>

                    {/*CATEGORIES*/}
                    <Route exact path="/categories" component={CategoriesList}/>
                    <Route exact path="/categories/:id" component={CategorieDetails}/>

                    {/*EDITIONS*/}
                    <Route exact path="/editions" component={EditionsList}/>
                    <Route exact path="/editions/:id" component={EditionDetails}/>

                    {/*AUTEURS*/}
                    <Route exact path="/auteurs/:id" component={AuteurDetails}/>

                    {/*LIVRES*/}
                    <Route exact path="/livres/add" component={LivresAdd}/>
                    <Route exact path="/livres/update/:id" component={LivresUpdate}/>


                </div>
            }
            <Route exact path="/" component={Home}/>

            {/*LOGIN | REGISTER*/}
            <Route exact path="/login" component={Login}/>

            {/*LIVRES*/}
            <Route exact path="/livres" component={LivresList}/>
            <Route exact path="/livres/details/:id" component={LivresDetails}/>

            {/*AUTEURS*/}
            <Route exact path="/auteurs" component={AuteursList}/>

        </BrowserRouter>
    }
}

const mapStateToProps = state =>{
    return {user: state.user}
};
export default connect(mapStateToProps,null)(App);