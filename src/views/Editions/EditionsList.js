import React, {Component} from 'react';
import EditionService from "../../services/edition.service";
import {Link} from "react-router-dom";

export default class EditionList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            editions: [],
            edition: null, //La nouvelle valeur du edition qu'on veut créer
            updatedEdition: {id: null, nom: null}
        }
    }

    async componentDidMount() {
        try{
            let response = await EditionService.list();
            this.setState({editions: response.data.editions});
        }catch (e) {
            console.error(e);
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeUpdate(e){
        this.setState({
            updatededition: {...this.state.updatedEdition, nom: e.target.value}
        });
    }

    async deleteEdition(id){
        try{
            //Delete le edition dans la base de données
            await EditionService.delete(id);

            //Rappel la liste des editions
            let response = await EditionService.list();
            this.setState({editions: response.data.editions});
        }catch (e) {
            console.error(e);
        }
    }

    async submit(e){
        e.preventDefault();
        try{
            //Créer le body
            let body = {nom: this.state.edition};
            //Execute l'appel d'API avec le body
            let response = await EditionService.create(body);

            //Récupérer le nouveau edition qui vient d'être créé et qui est renvoyé par l'API
            let newEdition = response.data.edition;
            //Extrait tous les editions du State
            let {editions} = this.state;
            //Ajoute aux editions le nouveau edition
            editions.push(newEdition);
            //Modifie le state pour ajouter le edition
            this.setState({editions, edition: ''});
        }catch (e) {
            console.error(e);
        }
    }

    async submitUpdate(e){
        e.preventDefault();
        try {
            let body = {nom: this.state.updatedEdition.nom};
            let response = await EditionService.update(this.state.updatedEdition.id, body);

            /**
             * Traitement pour ajouter dynamiquement ton nouveau edition dans le state
             */

        }catch (e) {
            console.error(e);
        }
    }

    handleClickBtnModal(edition){
        this.setState({updatedEdition: {id: edition._id, nom: edition.nom}});
    }

    render() {
        let {editions} = this.state;
        return (
            <div className="container-fluid">
                <h1>Liste des editions</h1>

                <table className="table">
                    <thead>
                        <tr>
                            <th>nom</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            editions.map((edition, index) => {
                                return <tr key={index}>
                                    <td>{edition.nom}</td>
                                    <td>

                                        <Link to={`/editions/${edition._id}`}>Update</Link>

                                        {/*<button type="button" className="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#modalUpdate"
                                            onClick={() => this.handleClickBtnModal(edition)}>U</button>*/}

                                        <button className="btn btn-danger" onClick={() => this.deleteEdition(edition._id)}>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                                 fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fillRule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>

                <hr/>
                <h2>Ajouter un edition</h2>

                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Edition</label>
                        <input type="text" id="edition" className="form-control"
                            required
                               value={this.state.edition}
                               onChange={(e) => this.handleChange(e)}/>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus" fill="currentColor"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd"
                                  d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg>
                    </button>
                </form>

                <div className="modal fade" id="modalUpdate" tabIndex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog">
                        <form onSubmit={(e) => this.submitUpdate(e)}>
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Update edition</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label>Edition</label>
                                        <input type="text" id="updatedEdition" className="form-control"
                                               required
                                               value={this.state.updatedEdition.nom}
                                               onChange={(e) => this.handleChangeUpdate(e)}/>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="submit" className="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>





            </div>
        );
    }
}
