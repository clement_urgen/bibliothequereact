import React, {Component} from 'react';
import GenreService from "../../services/genre.service";
import {Link} from "react-router-dom";

export default class GenresList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            genres: [],
            genre: null, //La nouvelle valeur du genre qu'on veut créer
            updatedGenre: {id: null, libelle: null}
        }
    }

    async componentDidMount() {
        try{
            let response = await GenreService.list();
            this.setState({genres: response.data.genres});
        }catch (e) {
            console.error(e);
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeUpdate(e){
        this.setState({
            updatedGenre: {...this.state.updatedGenre, libelle: e.target.value}
        });
    }

    async deleteGenre(id){
        try{
            //Delete le genre dans la base de données
            await GenreService.delete(id);

            //Rappel la liste des genres
            let response = await GenreService.list();
            this.setState({genres: response.data.genres});
        }catch (e) {
            console.error(e);
        }
    }

    async submit(e){
        e.preventDefault();
        try{
            //Créer le body
            let body = {libelle: this.state.genre};
            //Execute l'appel d'API avec le body
            let response = await GenreService.create(body);

            //Récupérer le nouveau genre qui vient d'être créé et qui est renvoyé par l'API
            let newGenre = response.data.genre;
            //Extrait tous les genres du State
            let {genres} = this.state;
            //Ajoute aux genres le nouveau genre
            genres.push(newGenre);
            //Modifie le state pour ajouter le genre
            this.setState({genres, genre: ''});
        }catch (e) {
            console.error(e);
        }
    }

    async submitUpdate(e){
        e.preventDefault();
        try {
            let body = {libelle: this.state.updatedGenre.libelle};
            let response = await GenreService.update(this.state.updatedGenre.id, body);

            /**
             * Traitement pour ajouter dynamiquement ton nouveau genre dans le state
             */

        }catch (e) {
            console.error(e);
        }
    }

    handleClickBtnModal(genre){
        this.setState({updatedGenre: {id: genre._id, libelle: genre.libelle}});
    }

    render() {
        let {genres} = this.state;
        return (
            <div className="container-fluid">
                <h1>Liste des genres</h1>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Libelle</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            genres.map((genre, index) => {
                                return <tr key={index}>
                                    <td>{genre.libelle}</td>
                                    <td>

                                        <Link to={`/genres/${genre._id}`}>Update</Link>

                                        {/*<button type="button" className="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#modalUpdate"
                                            onClick={() => this.handleClickBtnModal(genre)}>U</button>*/}

                                        <button className="btn btn-danger" onClick={() => this.deleteGenre(genre._id)}>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                                 fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fillRule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>

                <hr/>
                <h2>Ajouter un genre</h2>

                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Genre</label>
                        <input type="text" id="genre" className="form-control"
                            required
                               value={this.state.genre}
                               onChange={(e) => this.handleChange(e)}/>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus" fill="currentColor"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd"
                                  d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg>
                    </button>
                </form>

                <div className="modal fade" id="modalUpdate" tabIndex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog">
                        <form onSubmit={(e) => this.submitUpdate(e)}>
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Update genre</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label>Genre</label>
                                        <input type="text" id="updatedGenre" className="form-control"
                                               required
                                               value={this.state.updatedGenre.libelle}
                                               onChange={(e) => this.handleChangeUpdate(e)}/>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="submit" className="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>





            </div>
        );
    }
}
