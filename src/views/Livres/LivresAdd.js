import React, {Component} from 'react';
import GenreService from "../../services/genre.service";
import LivreService from "../../services/livre.service";
import AuteurService from "../../services/auteur.service";
import EditionService from "../../services/edition.service";
import CategorieService from "../../services/categorie.service";

export default class LivresAdd extends Component{

    constructor(props) {
        super(props);
        this.state = {
            titre: null,
            parution: null,
            synopsis: null,
            couverture: null,
            lien: null,

            activatedGenres: null,
            activatedAuteurs: null,
            activatedEditions: null,
            activatedCategories: null,

            genres: [],
            auteurs: [],
            editions: [],
            categories: [],
        }
    }

    async componentDidMount() {
        try{
            let responseGenre = await GenreService.list();
            let responseEdition = await EditionService.list();
            let responseCategorie = await CategorieService.list();
            let responseAuteur = await AuteurService.list();

            this.setState({
                genres: responseGenre.data.genres,
                activatedGenres: responseGenre.data.genres[0]._id
            });
            this.setState({
                editions: responseEdition.data.editions,
                activatedEditions: responseEdition.data.editions[0]._id
            });
            this.setState({
                categories: responseCategorie.data.categories,
                activatedCategories: responseCategorie.data.categories[0]._id
            })
            this.setState({
                auteurs: responseAuteur.data.auteurs,
                activatedAuteurs: responseAuteur.data.auteurs[0]._id
            });
        }catch (e) {
            console.error(e);
        }
    }


    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeThumbnail(e){
        this.setState({
            couverture: e.target.files[0]
        });
    }

    async submit(e){
        e.preventDefault();

        let {titre, parution, synopsis, couverture, lien, activatedGenres, activatedAuteurs, activatedEditions, activatedCategories} = this.state;

        let formData = new FormData();
        formData.append('titre', titre);
        formData.append('parution', parution);
        formData.append('synopsis', synopsis);
        formData.append('couverture', couverture);
        formData.append('lien', lien);
        formData.append('genres', [activatedGenres]);
        formData.append('auteur', [activatedAuteurs]);
        formData.append('edition', [activatedEditions]);
        formData.append('categorie', [activatedCategories]);

        try{
            await LivreService.create(formData);
            this.props.history.push('/livres');
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {genres, auteurs, editions, categories} = this.state;
        return <div className="container-fluid">
            <h1>Ajouter un Livre</h1>

            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Titre</label>
                    <input type="text" id="titre" required className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                </div>

                <div className="form-group">
                    <label>Auteur</label>
                    <select id="activatedAuteurs" required className="form-control"
                            onChange={(e) => this.handleChange(e)}>
                        {
                            auteurs.map((auteur, index) => {
                                return <option key={index} value={auteur._id}>{auteur.nom} {auteur.prenom}</option>
                            })
                        }
                    </select>
                </div>

                <div className="form-group">
                    <label>Synopsis</label>
                    <input type="text" id="synopsis" className="form-control"
                           onChange={(e) => this.handleChange(e)}/>
                </div>

                <div className="form-group">
                    <label>Edition</label>
                    <select id="activatedEditions" required className="form-control"
                            onChange={(e) => this.handleChange(e)}>
                        {
                            editions.map((edition, index) => {
                                return <option key={index} value={edition._id}>{edition.nom}</option>
                            })
                        }
                    </select>
                </div>

                <div className="form-group">
                    <label>Parution</label>
                    <input type="text" id="parution" required className="form-control"
                           onChange={(e) => this.handleChange(e)}/>
                </div>

                <div className="form-group">
                    <label>Couverture</label>
                    <input type="file" id="couverture" accept="image/jpeg, image/png"
                           required className="form-control"
                            onChange={(e) => this.handleChangeThumbnail(e)}/>
                </div>

                <div className="form-group">
                    <label>Lien</label>
                    <input type="url" id="lien" required className="form-control"
                           onChange={(e) => this.handleChange(e)}/>
                </div>


                <div className="row">

                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Genres</label>
                            <select id="activatedGenres" required className="form-control"
                                    onChange={(e) => this.handleChange(e)}>
                                {
                                    genres.map((genre, index) => {
                                        return <option key={index} value={genre._id}>{genre.libelle}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Categorie</label>
                            <select id="activatedCategories" required className="form-control"
                                    onChange={(e) => this.handleChange(e)}>
                                {
                                    categories.map((categorie, index) => {
                                        return <option key={index} value={categorie._id}>{categorie.libelle}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>

                </div>

                <button type="submit" className="btn btn-primary">Ajouter</button>
            </form>

        </div>
    }
}
