import React, {Component} from 'react';

export default class LivreCard extends Component{

    render() {
        let {title, picture, genres, categorie, id} = this.props;

        return <div className="card">
            {
                picture && <img className="card-img-top" id="cover" src={picture} alt="Card image cap"/>
            }
            <div className="card-body">
                <h2 className="card-title">{title}</h2>
                <div>
                    {
                        genres && genres.map((genre, index) => {
                            return <span key={index} className="mr-2 badge badge-secondary">{genre.libelle}</span>
                        })
                    }
                </div>

                <div>
                    {
                        categorie &&  <span  className="mr-2 badge badge-secondary">{categorie.libelle}</span>
                    }
                </div>

                <a href={`/livres/details/${id}`} className="btn btn-primary">Voir le livre </a>
            </div>
        </div>
    }
}
