## A propos
Le site bibliothèque premet au user de télécharger les livre de leur choix ainsi que visualiser les livre existant sur le site


## Installation
installation des libraries du package.json
npm install

npm run start

## Problèmes survenus
Prinda Sivakumaran:
   - Le token
   - problème au niveau de l'authenfication
   
Clément Urgen:
   - Update des images pour les Auteurs et les Livres (non résolu)
   - Update des livres et des auteurs
   - Affichage des images


## Les tâches effectuées
 Clément Urgen:
   - CRUD Auteur, Categorie, Editions, Genre
   - Read Update Delete des Livres 

 Prinda Sivakumaran:
   - Ajout des livres
   - Ajout de l'authenfication 
   - Définition des roles

