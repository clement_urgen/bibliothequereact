import axios from 'axios';

export default class EditionService{

    /**
     * Get edition's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/editions`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Create one edition
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/editions`, body,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Update one edition
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/editions/${id}`, body,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }

    /**
     * Delete one edition
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/editions/${id}`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenbibliotheque')}`
            }
        });
    }
}
