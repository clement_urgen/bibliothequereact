import React, {Component} from 'react';
import LivreService from "../../services/livre.service";
import moment from 'moment';
import {Link} from 'react-router-dom';

export default class LivresDétails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            livre: null,
            titre: null,
            parution: null,
            synopsis: null,
            couverture: null,
            lien: null,

            genres: [],
            auteurs: [],
            editions: [],
            categories: [],
        }
    }

    async componentDidMount() {
        try{
            let {id} = this.props.match.params;
            let response = await LivreService.list();
            let {livres} = response.data;
            let livre = livres.find(item => item._id === id);

            this.setState({livre: livre, 
                titre: livre.titre, 
                parution: livre.parution, 
                synopsis: livre.synopsis, 
                lien: livre.lien,
                couverture: livre.couverture,
                genres: livre.genres[0],
                editions: livre.edition[0],
                categories: livre.categorie[0],
                auteurs: livre.auteur[0],
            });
        }
        catch (e) {
            console.error(e);
        }
    }

    render() {
        let {livre, titre, parution, synopsis, couverture, lien, genres, auteurs, editions, categories} = this.state;
        return <div className="container-fluid">
                    <div className="row ">
                        <h1 className="livre">{titre}</h1>
                    </div>

                    <div className="row">
                        <img className="card-img-top img-details" src={`${process.env.REACT_APP_HOST_API}/${couverture}`} alt="Card image cap"/>
                    </div>

                    <div className="row">
                        <div className="col-2">
                            <label>Synopsis : </label>
                        </div>
                        <div className="col-8">
                            <p>{synopsis}</p>
                        </div>
                    </div>
                     <div className="row">
                        <div className="col-2">
                            <label>Auteur : </label>
                        </div>
                        <div className="col-8">
                            <p>{auteurs.prenom} {auteurs.nom}</p>
                        </div>
                    </div>
                     <div className="row">
                        <div className="col-2">
                            <label>Genre : </label>
                        </div>
                        <div className="col-8">
                            <p>{genres.libelle}</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2">
                            <label>Catégorie : </label>
                        </div>
                        <div className="col-8">
                            <p>{categories.libelle}</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-2">
                            <p>Maison d'édition : </p>
                        </div>
                        <div className="col-8">
                            <p>{editions.nom}</p>
                        </div>
                    </div>
                     <div className="row">
                         <div className="col-2">
                             <p>Date de parution : </p>
                         </div>
                         <div className="col-8">
                             <p>{moment(parution).format('D/M/Y')}</p>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-2">
                             <p>Lien de téléchargement : </p>
                         </div>
                         <div className="col-8">
                             <a href={lien}>Télécharger</a>
                         </div>
                     </div>
                </div>
    }
}
