import React, {Component} from 'react';
import CategorieService from "../../services/categorie.service";
import {Link} from "react-router-dom";

export default class CategoriesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            categorie: null, //La nouvelle valeur du categorie qu'on veut créer
            updatedCategorie: {id: null, libelle: null}
        }
    }

    async componentDidMount() {
        try{
            let response = await CategorieService.list();
            this.setState({categories: response.data.categories});
        }catch (e) {
            console.error(e);
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeUpdate(e){
        this.setState({
            updatedCategorie: {...this.state.updatedCategorie, libelle: e.target.value}
        });
    }

    async deleteCategorie(id){
        try{
            //Delete le categorie dans la base de données
            await CategorieService.delete(id);

            //Rappel la liste des categories
            let response = await CategorieService.list();
            this.setState({categories: response.data.categories});
        }catch (e) {
            console.error(e);
        }
    }

    async submit(e){
        e.preventDefault();
        try{
            //Créer le body
            let body = {libelle: this.state.categorie};
            //Execute l'appel d'API avec le body
            let response = await CategorieService.create(body);

            //Récupérer le nouveau categorie qui vient d'être créé et qui est renvoyé par l'API
            let newCategorie = response.data.categorie;
            //Extrait tous les categories du State
            let {categories} = this.state;
            //Ajoute aux categories le nouveau categorie
            categories.push(newCategorie);
            //Modifie le state pour ajouter le categorie
            this.setState({categories, categorie: ''});
        }catch (e) {
            console.error(e);
        }
    }

    async submitUpdate(e){
        e.preventDefault();
        try {
            let body = {libelle: this.state.updatedCategorie.libelle};
            let response = await CategorieService.update(this.state.updatedCategorie.id, body);

            /**
             * Traitement pour ajouter dynamiquement ton nouveau categorie dans le state
             */

        }catch (e) {
            console.error(e);
        }
    }

    handleClickBtnModal(categorie){
        this.setState({updatedCategorie: {id: categorie._id, libelle: categorie.libelle}});
    }

    render() {
        let {categories} = this.state;
        return (
            <div className="container-fluid">
                <h1>Liste des categories</h1>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Libelle</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            categories.map((categorie, index) => {
                                return <tr key={index}>
                                    <td>{categorie.libelle}</td>
                                    <td>

                                        <Link to={`/categories/${categorie._id}`}>Update</Link>

                                        {/*<button type="button" className="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#modalUpdate"
                                            onClick={() => this.handleClickBtnModal(categorie)}>U</button>*/}

                                        <button className="btn btn-danger" onClick={() => this.deleteCategorie(categorie._id)}>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                                 fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fillRule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>

                <hr/>
                <h2>Ajouter un categorie</h2>

                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Categorie</label>
                        <input type="text" id="categorie" className="form-control"
                            required
                               value={this.state.categorie}
                               onChange={(e) => this.handleChange(e)}/>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-plus" fill="currentColor"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd"
                                  d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                        </svg>
                    </button>
                </form>

                <div className="modal fade" id="modalUpdate" tabIndex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog">
                        <form onSubmit={(e) => this.submitUpdate(e)}>
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Update categorie</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label>Categorie</label>
                                        <input type="text" id="updatedCategorie" className="form-control"
                                               required
                                               value={this.state.updatedCategorie.libelle}
                                               onChange={(e) => this.handleChangeUpdate(e)}/>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="submit" className="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>





            </div>
        );
    }
}
