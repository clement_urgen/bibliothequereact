import React, {Component} from 'react';
import LivreCard from "../components/LivreCard";
import LivreService from "../services/livre.service";

export default class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
            livres: []
        }
    }

    async componentDidMount() {
        try{
            let response = await LivreService.list();
            this.setState({livres: response.data.livres});
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {livres} = this.state;
        return <div className="container-fluid">
            <h1>Bibliothèque</h1>
            <p>Bienvenue sur notre Bibliothèque en ligne !</p>

            <h2>Nos livres</h2>

            <div className="row">
                {
                    livres.map((livre, index) => {
                        return <div className="col-md-4 mb-3">
                            <LivreCard
                                key={index}
                                id={livre._id}
                                title={livre.titre}
                                genres={livre.genres}
                                categorie={livre.categorie}
                                picture={`${process.env.REACT_APP_HOST_API}/${livre.couverture}`}
                            />
                        </div>
                    })
                }
            </div>
        </div>
    }
}
