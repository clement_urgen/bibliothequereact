import React, {Component} from 'react';
import EditionService from "../../services/edition.service";

export default class EditionDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            edition: null,
            nom: null,
        }
    }

    async componentDidMount() {
        //Récupération de l'id passé dans l'URL
        let {id} = this.props.match.params;

        //Requête pour récupérer tous les editions
        let response = await EditionService.list();
        let {editions} = response.data;
        let edition = editions.find(item => item._id === id);
        this.setState({edition: edition, nom: edition.nom});
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    async submit(e){
        e.preventDefault();

        try{
            let id = this.state.edition._id;
            let body = {nom: this.state.nom};
            await EditionService.update(id, body);
            this.props.history.push('/editions');
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {edition, nom} = this.state;
        return <div className="container-fluid">
            <h1>Update de l'edition {edition && edition.nom}</h1>

            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Edition</label>
                    <input type="text"
                           id="nom"
                           required
                           value={nom && nom}
                           className="form-control"
                        onChange={(e) => this.handleChange(e)}/>
                </div>

                <button type="submit" className="btn btn-primary">Update</button>
            </form>

        </div>
    }

}
